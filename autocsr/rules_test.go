package autocsr

import (
	"crypto/x509"
	"crypto/x509/pkix"
	"net"
	"testing"

	certs "k8s.io/api/certificates/v1beta1"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestRequestCreatedWithPodServiceAccount(t *testing.T) {
	tests := []struct {
		name string
		cert *certs.CertificateSigningRequest
		csr  *x509.CertificateRequest
		pod  core.Pod

		want    bool
		wantErr bool
	}{
		{
			name: "returns true when the service account matches the pod",
			cert: &certs.CertificateSigningRequest{
				Spec: certs.CertificateSigningRequestSpec{
					Username: "system:serviceaccount:default:pod-service-account-name",
				},
			},
			pod: core.Pod{
				ObjectMeta: meta.ObjectMeta{
					Namespace: "default",
				},
				Spec: core.PodSpec{
					ServiceAccountName: "pod-service-account-name",
				},
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "returns false when the service account does not match the pod",
			cert: &certs.CertificateSigningRequest{
				Spec: certs.CertificateSigningRequestSpec{
					Username: "system:serviceaccount:default:pod-service-account-name",
				},
			},
			pod: core.Pod{
				ObjectMeta: meta.ObjectMeta{
					Namespace: "default",
				},
				Spec: core.PodSpec{
					ServiceAccountName: "another-service-account",
				},
			},
			want:    false,
			wantErr: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(tt *testing.T) {
			ok, err := RequestCreatedWithPodServiceAccount(test.cert, test.csr, test.pod)
			if ok != test.want {
				tt.Errorf("result is not what the test wanted. got %v wanted %v", ok, test.want)
			}
			if err != nil != test.wantErr {
				tt.Errorf("error is not what the test wanted. got %v wanted %v", err, test.wantErr)
			}
		})
	}
}

type FakeResolver struct {
	ips []net.IP
	err error
}

func (r *FakeResolver) LookupIP(host string) ([]net.IP, error) {
	return r.ips, r.err
}

func TestCommonNameResolvesToPod(t *testing.T) {
	tests := []struct {
		name string
		cert *certs.CertificateSigningRequest
		csr  *x509.CertificateRequest
		pod  core.Pod

		resolveIPs []net.IP
		resolveErr error

		want    bool
		wantErr bool
	}{
		{
			name: "returns true on the host resolving to the pod's ip",
			csr: &x509.CertificateRequest{
				Subject: pkix.Name{
					CommonName: "abc.123",
				},
			},
			pod: core.Pod{
				Status: core.PodStatus{
					PodIP: "1.2.3.4",
				},
			},
			resolveIPs: []net.IP{net.ParseIP("1.2.3.4")},
			want:       true,
			wantErr:    false,
		},
		{
			name: "returns true on and of the resolved ips matching the pod's ip",
			csr: &x509.CertificateRequest{
				Subject: pkix.Name{
					CommonName: "abc.123",
				},
			},
			pod: core.Pod{
				Status: core.PodStatus{
					PodIP: "5.6.7.8",
				},
			},
			resolveIPs: []net.IP{net.ParseIP("1.2.3.4"), net.ParseIP("5.6.7.8")},
			want:       true,
			wantErr:    false,
		},
		{
			name: "returns false when the hostname does not resolve to the pod",
			csr: &x509.CertificateRequest{
				Subject: pkix.Name{
					CommonName: "abc.123",
				},
			},
			pod: core.Pod{
				Status: core.PodStatus{
					PodIP: "5.6.7.8",
				},
			},
			resolveIPs: []net.IP{net.ParseIP("1.2.3.4")},
			want:       false,
			wantErr:    false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(tt *testing.T) {
			defaultResolver = &FakeResolver{
				ips: test.resolveIPs,
				err: test.resolveErr,
			}
			ok, err := CommonNameResolvesToPod(test.cert, test.csr, test.pod)
			if ok != test.want {
				tt.Errorf("result is not what the test wanted. got %v wanted %v", ok, test.want)
			}
			if err != nil != test.wantErr {
				tt.Errorf("error is not what the test wanted. got %v wanted %v", err, test.wantErr)
			}
		})
	}
}

func TestDNSNamesResolveToPod(t *testing.T) {
	tests := []struct {
		name string
		cert *certs.CertificateSigningRequest
		csr  *x509.CertificateRequest
		pod  core.Pod

		resolveIPs []net.IP
		resolveErr error

		want    bool
		wantErr bool
	}{
		{
			name: "returns true on the host resolving to the pod's ip",
			csr: &x509.CertificateRequest{
				DNSNames: []string{
					"abc.123",
					"def.123",
				},
			},
			pod: core.Pod{
				Status: core.PodStatus{
					PodIP: "1.2.3.4",
				},
			},
			resolveIPs: []net.IP{net.ParseIP("1.2.3.4")},
			want:       true,
			wantErr:    false,
		},
		{
			name: "returns true on and of the resolved ips matching the pod's ip",
			csr: &x509.CertificateRequest{
				DNSNames: []string{
					"abc.123",
					"def.123",
				},
			},
			pod: core.Pod{
				Status: core.PodStatus{
					PodIP: "5.6.7.8",
				},
			},
			resolveIPs: []net.IP{net.ParseIP("1.2.3.4"), net.ParseIP("5.6.7.8")},
			want:       true,
			wantErr:    false,
		},
		{
			name: "returns false when the hostname does not resolve to the pod",
			csr: &x509.CertificateRequest{
				DNSNames: []string{
					"abc.123",
					"def.123",
				},
			},
			pod: core.Pod{
				Status: core.PodStatus{
					PodIP: "5.6.7.8",
				},
			},
			resolveIPs: []net.IP{net.ParseIP("1.2.3.4")},
			want:       false,
			wantErr:    false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(tt *testing.T) {
			defaultResolver = &FakeResolver{
				ips: test.resolveIPs,
				err: test.resolveErr,
			}
			ok, err := DNSNamesResolveToPod(test.cert, test.csr, test.pod)
			if ok != test.want {
				tt.Errorf("result is not what the test wanted. got %v wanted %v", ok, test.want)
			}
			if err != nil != test.wantErr {
				tt.Errorf("error is not what the test wanted. got %v wanted %v", err, test.wantErr)
			}
		})
	}
}
