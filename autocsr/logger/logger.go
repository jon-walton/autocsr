package logger

import (
	"os"

	"github.com/sirupsen/logrus"
)

// These will be updated during CI build.
var (
	Commit = "GIT"
)

var Log *logrus.Entry

type Fields = logrus.Fields

func init() {
	log := logrus.Logger{
		Level:     logrus.DebugLevel,
		Out:       os.Stderr,
		Formatter: new(logrus.TextFormatter),
	}

	if Commit != "GIT" {
		log.Formatter = &logrus.JSONFormatter{}
	}

	Log = log.WithFields(Fields{
		"commit": Commit,
	})
}
