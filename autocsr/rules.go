package autocsr

import (
	"crypto/x509"
	"fmt"

	"github.com/pkg/errors"
	"gitlab.com/jon-walton/autocsr/autocsr/logger"
	certs "k8s.io/api/certificates/v1beta1"
	core "k8s.io/api/core/v1"
)

type RuleFunc func(*certs.CertificateSigningRequest, *x509.CertificateRequest, core.Pod) (bool, error)

func execRules(cert *certs.CertificateSigningRequest, csr *x509.CertificateRequest, pod core.Pod, rules []RuleFunc) (bool, error) {
	for _, rule := range rules {
		ok, err := rule(cert, csr, pod)
		if err != nil {
			return false, errors.Wrap(err, "error executing rule")
		}
		if !ok {
			return false, nil
		}
	}
	return true, nil
}

func RequestCreatedWithPodServiceAccount(cert *certs.CertificateSigningRequest, _ *x509.CertificateRequest, pod core.Pod) (bool, error) {
	serviceAccountName := fmt.Sprintf("system:serviceaccount:%v:%v", pod.Namespace, pod.Spec.ServiceAccountName)
	match := cert.Spec.Username == serviceAccountName

	logger.Log.WithFields(logger.Fields{
		"pod":                pod.Name,
		"username":           cert.Spec.Username,
		"serviceAccountName": serviceAccountName,
		"match":              match,
	}).Debug("RequestCreatedWithPodServiceAccount")

	return match, nil
}

func CommonNameResolvesToPod(_ *certs.CertificateSigningRequest, req *x509.CertificateRequest, pod core.Pod) (bool, error) {
	ok, err := resolveHostToPod(req.Subject.CommonName, pod)

	logger.Log.WithFields(logger.Fields{
		"pod":        pod.Name,
		"commonName": req.Subject.CommonName,
		"match":      ok,
		"error":      err,
	}).Debug("CommonNameResolvesToPod")

	return ok, errors.Wrap(err, "CommonNameResolvesToPod: error resolving host to pod")
}

func DNSNamesResolveToPod(_ *certs.CertificateSigningRequest, req *x509.CertificateRequest, pod core.Pod) (bool, error) {
	ok, err := resolveHostsToPod(req.DNSNames, pod)

	logger.Log.WithFields(logger.Fields{
		"pod":      pod.Name,
		"DNSNames": req.DNSNames,
		"match":    ok,
		"error":    err,
	}).Debug("DNSNamesResolveToPod")

	return ok, errors.Wrap(err, "DNSNamesResolveToPod: error resolving DNSNames to pod")
}
