package autocsr

import (
	"crypto/x509"
	"encoding/pem"

	"github.com/pkg/errors"
	certs "k8s.io/api/certificates/v1beta1"
)

func parseCSR(cert *certs.CertificateSigningRequest) (*x509.CertificateRequest, error) {
	block, _ := pem.Decode(cert.Spec.Request)
	if block == nil || block.Type != "CERTIFICATE REQUEST" {
		return nil, errors.New("PEM block type must be CERTIFICATE REQUEST")
	}
	csr, err := x509.ParseCertificateRequest(block.Bytes)
	if err != nil {
		return nil, err
	}
	return csr, nil
}
