package autocsr

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"gitlab.com/jon-walton/autocsr/autocsr/testhelpers"
	"k8s.io/api/certificates/v1beta1"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestApproveCert(t *testing.T) {
	client := testhelpers.CreateClient(t)

	tests := []struct {
		Name         string
		WantApproval bool
		WantErr      bool

		PodName string
		Args    []string
	}{
		{
			Name:         "successfully approves the csr",
			WantApproval: true,
			WantErr:      false,
			PodName:      "successful",
			Args: []string{
				"-cert-dir=/tmp/",
				"-namespace=$(NAMESPACE)",
				"-pod-ip=$(POD_IP)",
				"-pod-name=$(POD_NAME)",
			},
		},

		{
			Name:         "does not approve if the CommonName does not resolve to the pod",
			WantApproval: false,
			WantErr:      false,
			PodName:      "wrongdns",
			Args: []string{
				"-cert-dir=/tmp/",
				"-namespace=$(NAMESPACE)",
				"-pod-ip=$(POD_IP)",
				"-pod-name=$(POD_NAME)",
				"-cluster-domain=wrong.cluster.local",
			},
		},
	}

	for _, test := range tests {
		t.Run(test.Name, func(tt *testing.T) {
			ns, sa := testhelpers.Prepare(tt, client)
			defer testhelpers.Cleanup(tt, client, ns)

			// create the pod to submit the request
			pod, err := client.CoreV1().Pods(ns).Create(&core.Pod{
				ObjectMeta: meta.ObjectMeta{
					Name:      fmt.Sprintf("%v-%v", test.PodName, rand.Int63()),
					Namespace: ns,
				},
				Spec: core.PodSpec{
					RestartPolicy:      core.RestartPolicyNever,
					ServiceAccountName: sa,
					Containers: []core.Container{
						{
							Name:            test.PodName,
							Image:           "gcr.io/hightowerlabs/certificate-init-container:0.0.1",
							ImagePullPolicy: core.PullIfNotPresent,
							Env: []core.EnvVar{
								{
									Name:  "NAMESPACE",
									Value: ns,
								},
								{
									Name: "POD_NAME",
									ValueFrom: &core.EnvVarSource{
										FieldRef: &core.ObjectFieldSelector{
											FieldPath: "metadata.name",
										},
									},
								},
								{
									Name: "POD_IP",
									ValueFrom: &core.EnvVarSource{
										FieldRef: &core.ObjectFieldSelector{
											FieldPath: "status.podIP",
										},
									},
								},
							},
							Args: test.Args,
						},
					},
				},
			})
			if err != nil {
				tt.Fatalf("error creating cert-req pod: %v", err.Error())
			}

			// check
			start := time.Now()
			timeout := 30 * time.Second
		LOOP:
			for {
				p, err := client.CoreV1().Pods(ns).Get(pod.Name, meta.GetOptions{})
				if err != nil {
					tt.Fatalf("error receiving pod status: %v", err.Error())
				}

				switch p.Status.Phase {
				case core.PodFailed:
					tt.Fatalf("error starting pod: %v", p.Status.Reason)

				case core.PodRunning:
					break LOOP

				case core.PodPending:
					if time.Now().After(start.Add(timeout)) {
						tt.Fatal("timed out waiting for the pod to be created")
					}
					time.Sleep(100 * time.Millisecond)
					continue LOOP

				default:
					tt.Fatalf("pod in an unknown state: %v", p.Status.Phase)
				}
				break
			}

			start = time.Now()
			timeout = 10 * time.Second
			var csr *v1beta1.CertificateSigningRequest
			for {
				name := fmt.Sprintf("%v-%v", pod.Name, ns)
				csr, err = client.CertificatesV1beta1().CertificateSigningRequests().Get(name, meta.GetOptions{})
				if err != nil {
					if time.Now().After(start.Add(timeout)) {
						tt.Fatalf("error listing certificate signing requests: %v", err.Error())
					}
					tt.Log("csr not yet created...")
					time.Sleep(100 * time.Millisecond)
					continue
				}
				break
			}

			pods, err := client.CoreV1().Pods(ns).List(meta.ListOptions{})
			if err != nil {
				tt.Fatalf("error listing pods: %v", err.Error())
			}

			rules := []RuleFunc{
				RequestCreatedWithPodServiceAccount,
				CommonNameResolvesToPod,
				DNSNamesResolveToPod,
			}
			ok, err := ApproveCert(csr, pods.Items, rules)
			if err != nil != test.WantErr {
				tt.Errorf("unexpected error state. got %v wanted %v", err, test.WantErr)
			}
			if ok != test.WantApproval {
				tt.Errorf("unexpected result. got %v wanted %v", ok, test.WantApproval)
			}
		})
	}
}
