package autocsr

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/jon-walton/autocsr/autocsr/logger"
	certs "k8s.io/api/certificates/v1beta1"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

// WatchCSR will continuously watch for incoming certificate signing requests and process them
// until the context has been cancelled
func WatchCSR(ctx context.Context, client *kubernetes.Clientset) error {
	csrClient := client.CertificatesV1beta1().CertificateSigningRequests()
	podClient := client.CoreV1().Pods("") // all namespaces

	wait := time.Millisecond // this get's changed to something sensible later
	for {
		select {
		case <-ctx.Done():
			return nil

		// todo: switch to using Watch().
		case <-time.After(wait):
			// we don't want to wait before we do the first loop, but let's not hammer the api afterwards...
			wait = 30 * time.Second

			// todo: make use of ResourceVersion to only request what's changed
			csr, err := csrClient.List(meta.ListOptions{})
			if err != nil {
				logger.Log.WithError(err).Error("error listing certificate signing requests")
				break
			}

			// todo: make use of ResourceVersion to only request what's changed
			pods, err := podClient.List(meta.ListOptions{})
			if err != nil {
				logger.Log.WithError(err).Error("error listing pods")
				break
			}

			for _, cert := range csr.Items {
				select {
				case <-ctx.Done():
					return nil

				default:
					rules := []RuleFunc{
						RequestCreatedWithPodServiceAccount,
						CommonNameResolvesToPod,
						DNSNamesResolveToPod,
					}
					ok, err := ApproveCert(&cert, pods.Items, rules)
					if err != nil {
						logger.Log.WithError(err).WithField("cert", cert.Name).Error("error processing certificate")
						continue
					}
					if !ok {
						continue // putting it here to avoid multiple log entries
					}

					logger.Log.WithField("cert", cert.Name).Debug("approving csr")

					cert.Status.Conditions = append(cert.Status.Conditions, certs.CertificateSigningRequestCondition{
						Type:    certs.CertificateApproved,
						Message: "Approved by autocsr",
					})
					if _, err = csrClient.UpdateApproval(&cert); err != nil {
						logger.Log.WithError(err).WithField("cert", cert.Name).Error("error approving certificate")
						continue
					}
				}
			}
		}
	}

	return nil
}

// ApproveCert attempts to verify that the pod creating the CSR is authorised to do so.
//
// https://kubernetes.io/docs/tasks/tls/managing-tls-in-a-cluster/#approving-certificate-signing-requests
// Whether a machine or a human using kubectl as above, the role of the approver is to verify that the CSR satisfies two requirements:
//
// 1. The subject of the CSR controls the private key used to sign the CSR.
//   This addresses the threat of a third party masquerading as an authorized subject. In the above example,
//   this step would be to verify that the pod controls the private key used to generate the CSR.
//
// 2. The subject of the CSR is authorized to act in the requested context.
//   This addresses the threat of an undesired subject joining the cluster. In the above example,
//   this step would be to verify that the pod is allowed to participate in the requested service
func ApproveCert(cert *certs.CertificateSigningRequest, pods []core.Pod, rules []RuleFunc) (bool, error) {
	logger.Log.WithField("cert", cert.Name).Debug("processing cert")

	// pending certificates have an empty conditions slice.
	// we should check the conditions slice because the certificate is not issued immediately
	if len(cert.Status.Conditions) > 0 {
		logger.Log.WithFields(logger.Fields{
			"cert":       cert.Name,
			"conditions": cert.Status.Conditions,
		}).Debug("ignored certificate due to existing conditions")
		return false, nil
	}

	csr, err := parseCSR(cert)
	if err != nil {
		return false, errors.Wrapf(err, "error parsing CSR: %v", cert.Name)
	}
	if err = csr.CheckSignature(); err != nil {
		return false, errors.Wrapf(err, "csr signature is invalid: %v", cert.Name)
	}

	for _, pod := range pods {
		ok, err := execRules(cert, csr, pod, rules)
		if err != nil {
			logger.Log.WithError(err).WithField("pod", pod.Name).Error("error executing rules")
			continue
		}
		if !ok {
			continue
		}

		return true, nil
	}

	return false, nil
}
