package autocsr

import (
	"net"

	"github.com/pkg/errors"
	"gitlab.com/jon-walton/autocsr/autocsr/logger"
	core "k8s.io/api/core/v1"
)

type resolver interface {
	LookupIP(host string) ([]net.IP, error)
}

var defaultResolver resolver

type netResolver struct{}

func (r *netResolver) LookupIP(host string) ([]net.IP, error) {
	return net.LookupIP(host)
}

func init() {
	defaultResolver = &netResolver{}
}

// resolveHostToPod returns true if the hostname resolves to the pod's ip
func resolveHostToPod(host string, pod core.Pod) (bool, error) {
	ips, err := defaultResolver.LookupIP(host)
	if err != nil {
		return false, errors.Wrapf(err, "error resolving hostname: %v", host)
	}
	for _, ip := range ips {
		if ip.String() == pod.Status.PodIP {
			logger.Log.WithFields(logger.Fields{
				"host":  host,
				"pod":   pod.Name,
				"ip":    ip.String(),
				"podIP": pod.Status.PodIP,
			}).Debug("resolveHostToPod found a match")
			return true, nil
		}
	}
	return false, nil
}

// resolveHostsToPod returns true if all hosts resolve to the pod's ip
func resolveHostsToPod(hosts []string, pod core.Pod) (bool, error) {
	for _, host := range hosts {
		ok, err := resolveHostToPod(host, pod)
		if err != nil {
			return false, errors.Wrapf(err, "error resolving host: %v", host)
		}
		if !ok {
			return false, nil
		}
	}
	return true, nil
}
