package testhelpers

import (
	"fmt"
	"math/rand"
	"os"
	"testing"
	"time"

	core "k8s.io/api/core/v1"
	rbac "k8s.io/api/rbac/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

func CreateClient(t *testing.T) *kubernetes.Clientset {
	config := &rest.Config{
		Username: os.Getenv("k8s_user"),
		Password: os.Getenv("k8s_pass"),
		Host:     os.Getenv("k8s_host"),
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
			//CAData: []byte(os.Getenv("k8s_cert")),
		},
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		t.Fatalf("error creating kubernetes clientset: %v", err.Error())
	}
	return clientset
}

// Prepare creates the minimal resources required for the tests and returns the newly created namespace
func Prepare(t *testing.T, client *kubernetes.Clientset) (string, string) {
	rand.Seed(time.Now().UnixNano())
	ns, err := client.CoreV1().Namespaces().Create(&core.Namespace{
		ObjectMeta: meta.ObjectMeta{
			Name: fmt.Sprintf("%v", rand.Int63()),
		},
	})
	if err != nil {
		t.Fatalf("error creating namespace: %v", err.Error())
	}

	sa, err := client.CoreV1().ServiceAccounts(ns.Name).Create(&core.ServiceAccount{
		ObjectMeta: meta.ObjectMeta{
			Name:      fmt.Sprintf("certificate-init-service-account-%v", ns.Name),
			Namespace: ns.Name,
		},
	})
	if err != nil {
		Cleanup(t, client, ns.Name)
		t.Fatalf("error creating certificate-init-service-account: %v", err.Error())
	}

	cr, err := client.RbacV1().ClusterRoles().Create(&rbac.ClusterRole{
		ObjectMeta: meta.ObjectMeta{
			Name:      fmt.Sprintf("certificate-init-service-account-role-%v", ns.Name),
			Namespace: ns.Name,
		},
		Rules: []rbac.PolicyRule{
			{
				APIGroups: []string{"certificates.k8s.io"},
				Resources: []string{"certificatesigningrequests"},
				Verbs:     []string{"create", "get"},
			},
		},
	})
	if err != nil {
		Cleanup(t, client, ns.Name)
		t.Fatalf("error creating certificate-init-service-account-role: %v", err.Error())
	}

	_, err = client.RbacV1().ClusterRoleBindings().Create(&rbac.ClusterRoleBinding{
		ObjectMeta: meta.ObjectMeta{
			Name:      fmt.Sprintf("certificate-init-role-binding-%v", ns.Name),
			Namespace: ns.Name,
		},
		Subjects: []rbac.Subject{
			{
				Kind:      "ServiceAccount",
				Name:      sa.Name,
				Namespace: ns.Name,
			},
		},
		RoleRef: rbac.RoleRef{
			Kind:     "ClusterRole",
			Name:     cr.Name,
			APIGroup: "rbac.authorization.k8s.io",
		},
	})
	if err != nil {
		Cleanup(t, client, ns.Name)
		t.Fatalf("error creating certificate-init-role-binding: %v", err.Error())
	}

	return ns.Name, sa.Name
}

func Cleanup(t *testing.T, client *kubernetes.Clientset, namespace string) {
	if err := client.CoreV1().Namespaces().Delete(namespace, &meta.DeleteOptions{}); err != nil {
		t.Errorf("error cleaning up namespace %v: %v", namespace, err.Error())
	}
}

func newInt32(n int32) *int32 {
	return &n
}
