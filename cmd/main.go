package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/jon-walton/autocsr/autocsr"
	"gitlab.com/jon-walton/autocsr/autocsr/logger"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

func main() {
	logger.Log.Info("starting autocsr")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// trap any OS signals telling us to quit.
	// if received, cancelling the context will be enough to stop all processing and cleanly exit
	sig := make(chan os.Signal, 2)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-sig
		logger.Log.Info("received shutdown signal")
		cancel()
	}()

	// Initialise configuration
	config, err := rest.InClusterConfig()
	if err != nil {
		logger.Log.WithError(err).Fatal("error reading cluster config")
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		logger.Log.WithError(err).Fatal("error creating clientset")
	}

	err = autocsr.WatchCSR(ctx, clientset)
	if err != nil {
		logger.Log.WithError(err).Fatal("error watching for certificate requests")
	}
	logger.Log.Info("completed shutdown")
}
