# AutoCSR

AutoCSR is a service that monitors kubernetes certificate signing requests and approves them under the following conditions:

- The CommonName resolves to the POD that created the signing request
- The POD is running under the same service account that created the signing request
- The SAN domains all resolve to the POD

## Testing

Currently, local integration testing is not fleshed out and fails due to the local machine not being able to resolve DNS
from inside Kubernetes. Each commit pushed to GitLab is automatically tested (see [.gitlab-ci.yml](.gitlab-ci.yml))

## Deploying

GitlabCI automatically builds the `:latest` image upon successfully testing the master branch.

Deploy via kubectl

```
kubectl create -f ./k8s/autocsr.yaml
```

Upgrade

```
kubectl set image deployment/auto-csr-approval approver=registry.gitlab.com/jon-walton/autocsr:5ad9dd0af44f955af2a5c517e07eb81b24fbf13c
```

## Security

### Potential for PODs to create a CSR that it should not be authorised for

When creating the CSR, the client is not able to specify "who" is the owner. The ownership information is provided
by the Kubernetes backend. After resolving the CommonName to a POD, AutoCSR compares the CSR owner against the service account
the POD is running under. The application in the container cannot influence either the CSR owner or the POD service account,
therefore we are reasonably confident that the POD creating the CSR is also the one the CommonName resolves to.

### AutoCSR does not deny invalid requests

This may cause the number of pending certificate requests to increase over time. Manual occasional review is suggested.