FROM golang:1.10.0 as builder
WORKDIR /go/src/gitlab.com/jon-walton/autocsr/

# Dependencies. These are unlikely to change, so let's do them first
COPY Gopkg.* /go/src/gitlab.com/jon-walton/autocsr/
RUN go get -v github.com/golang/dep/cmd/dep
RUN dep ensure -v -vendor-only

COPY . /go/src/gitlab.com/jon-walton/autocsr/
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/autocsr cmd/main.go

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/jon-walton/autocsr/build/autocsr .
CMD ["./autocsr"]
